## Copyright (C) 2021 Alexandre
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <https://www.gnu.org/licenses/>.

## -*- texinfo -*-
## @deftypefn {} {@var{retval} =} newFiltre (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: Alexandre <Alexandre@DESKTOP-731OQFI>
## Created: 2021-03-10

function retval = newFiltre (img)
  
img2 = double(img);
[m,n]=size(img2);
for i= 2:m-1
  for j=2:n-1
    
    v = img2(i-1:i+1,j-1,j+1);
    img_out(i,j) = median(v(:));
    
  endfor
endfor

retval = uint8(img_out);

endfunction
