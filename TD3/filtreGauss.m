clear;
clc;
clear all;

N=3;
sig=0.5;


img = imread("lena.jpg");
subplot(2,1,1);
imshow(img);
centre = (N+1)/2;

x=centre;
y=centre;

for i=1:N
  for j=1:N
     
    M(i,j) = (1/(2*pi*power(sig,2)))*exp(-power(x,2)+-power(y,2)/2*power(sig,2));
    x = i-centre;
    y = j-centre;
    
  endfor
endfor

K = M /sum(sum(M));

img_out = imfilter(img,K);
subplot(2,1,2);
imshow(img_out);
