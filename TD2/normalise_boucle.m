## Copyright (C) 2021 Alexandre
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <https://www.gnu.org/licenses/>.

## -*- texinfo -*-
## @deftypefn {} {@var{retval} =} normalise_boucle (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: Alexandre <Alexandre@DESKTOP-731OQFI>
## Created: 2021-02-02

function retval = normalise_boucle (input1)

  img2 = double(input1);
  minI = min(min(img2));
  maxI = max(max(img2));
  
  #img3 = img2(:,:);
  [m,n] = size(img2);
  
  
  for x=1:m
    for y=1:n
      
      img3(x,y) = 255/(maxI - minI) * (img2(x,y) - minI);
      
    endfor
  endfor

  retval = uint8(img3);


endfunction
