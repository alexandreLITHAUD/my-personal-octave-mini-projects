clear;
clc;
clear all;

img = imread('landscape.jpg');
vignette = imread("vignette.png");
noise = imread("noise.png");

figure(1); imshow(img);
figure(2);imhist(img);

img2 = (vignette+img) /2;
img2 = (img2+noise) /2;

img2 = sepia(img2);



figure(3); imshow(img2);
figure(4);imhist(img2);  