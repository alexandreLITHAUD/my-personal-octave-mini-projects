## Copyright (C) 2021 Alexandre
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <https://www.gnu.org/licenses/>.

## -*- texinfo -*-
## @deftypefn {} {@var{retval} =} sepia (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: Alexandre <Alexandre@DESKTOP-731OQFI>
## Created: 2021-03-02

function img_out = sepia (img)
R= img(:,:,1);
G = img(:,:,2);
B = img(:,:,3);

r = (R*0.393+G*0.769+B*0.189);
g = (R*0.349+G*0.686+B*0.168);
b = (R*0.272+G*0.534+B*0.131);

#img_out = img;
img(:,:,1)=r;
img(:,:,2)=g;
img(:,:,3)=b;

#img = 255*img/max(img(:));

img_out = uint8(img);



endfunction
