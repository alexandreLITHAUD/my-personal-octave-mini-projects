clear;
clc;
clear all;

img = imread("lena.jpg");

a = min(min(img));
b = max(max(img));

Img1 = 255*(img-a)/(b-a);
Img_out = uint8(Img1);

figure(2);
imshow(Img_out);