## Copyright (C) 2021 Alexandre
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <https://www.gnu.org/licenses/>.

## -*- texinfo -*-
## @deftypefn {} {@var{retval} =} puissance (@var{x}, @var{n})
##
## @seealso{}
## @end deftypefn

## Author: Alexandre <Alexandre@DESKTOP-731OQFI>
## Created: 2021-01-31

function retval = puissance (x, n)

  temp = 1;
  
  for i=1:n
    temp = temp * x;
  endfor
  
  retval = temp;


endfunction
