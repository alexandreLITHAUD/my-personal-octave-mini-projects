clear;
clc;
clear all;

% On lit l'image du chien
img = imread('dog.png');
img2 = imread('fruits_ng.jpg');
img3 = imread('fruits.jpg');

% On affiche l'image du chien
figure(1);imshow(img);

% On lit le nombre de pixel de l'image
% px = size(img)

[m, n]=size(img)
NbPixels=m*n

% On affiche le nombre de pixel de l'image 
% disp(px);

% On affiche l'image avec des nuance de gris
figure(2); imshow(rgb2gray(img));

% 
nb = find(img2,150);

% calcule le nombre de pixel avec 150 en niveau de gris
Nb=sum(sum(img2(:,:)==150))

%
R = img3(:,:,1);
G = img3(:,:,2);
B = img3(:,:,3);
% disp(R);
% disp(G);
% disp(B);

%carr� rouge sur les fruits
for i=50:60
  for j=50:60
    img3(i,j,1) = 255;
    img3(i,j,2) = 0;
    img3(i,j,3) = 0;
  endfor
endfor

%enregistre l'image modifi� des fruits
imwrite(img3, 'test_image.jpg');


