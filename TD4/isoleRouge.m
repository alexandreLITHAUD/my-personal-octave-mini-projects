## Copyright (C) 2021 Alexandre
##
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <https://www.gnu.org/licenses/>.

## -*- texinfo -*-
## @deftypefn {} {@var{retval} =} isoleRouge (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: Alexandre <Alexandre@DESKTOP-731OQFI>
## Created: 2021-03-10

function retval = isoleRouge (input1)

img = double(input1);
[m,n] = size(img)

for x=1:321
  for y=1:517
    
    if(img(x,y,1) > 220)
    
       img(x,y,1) = 255;
       img(x,y,2) = 255;
       img(x,y,3) = 255;
       
    else
       img(x,y,1) = 0;
       img(x,y,2) = 0;
       img(x,y,3) = 0;
    endif
    
  endfor
endfor
retval = uint8(img);
endfunction
